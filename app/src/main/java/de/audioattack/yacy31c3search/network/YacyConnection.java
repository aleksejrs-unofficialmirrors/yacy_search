package de.audioattack.yacy31c3search.network;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

/**
 * @author Marc Nause <marc.nause@gmx.de>
 */
public class YacyConnection {


    public static HttpURLConnection getConnection(final URL url) throws IOException {

        final HttpURLConnection connection;
        final String protocol = url.getProtocol();

        if (protocol != null) {

            switch (protocol) {

                case "https":
                    connection = (HttpsURLConnection) url.openConnection();
                    break;

                case "http":
                    connection = (HttpURLConnection) url.openConnection();
                    break;

                default:
                    throw new IllegalArgumentException("Unknown protocol: " + protocol);
            }

        } else {

            throw new IllegalArgumentException("Undefined protocol: null");
        }

        connection.setReadTimeout(10000 /* milliseconds */);
        connection.setConnectTimeout(15000 /* milliseconds */);
        connection.setRequestMethod("GET");
        connection.setDoInput(true);

        return connection;
    }
}
