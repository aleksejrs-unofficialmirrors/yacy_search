package de.audioattack.yacy31c3search.ui.NavDrawer;

import de.audioattack.yacy31c3search.R;

/**
 * @author Marc Nause <marc.nause@gmx.de>
 */
public class DrawerItems {

    public static DrawerItem[] drawerItems = {
            new DrawerItem(R.string.action_settings, R.drawable.lb_ic_in_app_search),
            new DrawerItem(R.string.action_about, R.drawable.lb_ic_in_app_search)
    };
}
